package com.bkkami;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.MissingFormatArgumentException;

@RunWith(Parameterized.class)
public class OneCaseTest {
    private String input;

    public OneCaseTest(String input) {
        this.input = input;
    }
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"0"},
                {"15"},
                {"244"},
                {"5125"}
        });
    }

    @Test (expected = MissingFormatArgumentException.class)
    public void testOneCase(){
        Main.sortArray(input);
    }
}
