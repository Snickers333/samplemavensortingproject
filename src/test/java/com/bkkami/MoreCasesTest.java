package com.bkkami;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.MissingFormatArgumentException;

@RunWith(Parameterized.class)
public class MoreCasesTest {
    private String input;

    public MoreCasesTest(String input) {
        this.input = input;
    }
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"0 5 10 4 68 41 23 49 8 11 25 98 45 45 6 3 5 48 4"},
                {"24 3 11 9 87 54 252 6 48 96 21"},
                {"8 9 6 5 4 8 25 5 4 6 11 24 6 589 68"},
                {"8 5 13 4 6 0 23 24 87 11 15 17 82 45"}
        });
    }

    @Test  (expected = MissingFormatArgumentException.class)
    public void testTenCase(){
        Main.sortArray(input);
    }
}
