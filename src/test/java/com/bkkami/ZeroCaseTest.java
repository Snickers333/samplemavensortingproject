package com.bkkami;
import org.junit.Test;

import java.util.MissingFormatArgumentException;

public class ZeroCaseTest {
    @Test (expected = MissingFormatArgumentException.class)
    public void testZeroCase(){
        Main.sortArray("");
    }
}
