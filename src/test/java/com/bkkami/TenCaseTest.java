package com.bkkami;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class TenCaseTest {
    private String input;
    private String expected;

    public TenCaseTest(String input, String expected) {
        this.input = input;
        this.expected = expected;
    }
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"0 5 10 4 68 41 23 49 8 11", "[0, 4, 5, 8, 10, 11, 23, 41, 49, 68]"},
                {"24 3 11 9 87 54 252 6 48 96", "[3, 6, 9, 11, 24, 48, 54, 87, 96, 252]"},
                {"8 9 6 5 4 8 25 5 4 6", "[4, 4, 5, 5, 6, 6, 8, 8, 9, 25]"},
                {"8 5 13 4 6 0 23 24 87 11", "[0, 4, 5, 6, 8, 11, 13, 23, 24, 87]"}
        });
    }

    @Test
    public void testTenCase(){
        assertEquals(expected, Main.sortArray(input));
    }
}
