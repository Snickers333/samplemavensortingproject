package com.bkkami;

import java.util.Arrays;
import java.util.MissingFormatArgumentException;
import java.util.Scanner;

public class Main {
    public static void main( String[] args ) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter TEN numbers seperated by a space");
        System.out.println(sortArray(scanner.nextLine()));
    }

    public static String sortArray(String str) {
        int[] tab = new int[10];

        if (str.split(" ").length != 10){
            throw new MissingFormatArgumentException("");
        }

        String[] values = str.split(" ");

        for (int i = 0; i < tab.length; i++) {
            tab[i] = Integer.parseInt(values[i]);
        }

        int tmp = 0;
        for (int i = 0; i < tab.length; i++) {
            for (int j = i+1; j < tab.length; j++) {
                if(tab[i] > tab[j]) {
                    tmp = tab[i];
                    tab[i] = tab[j];
                    tab[j] = tmp;
                }
            }
        }

        return Arrays.toString(tab);
    }
}
